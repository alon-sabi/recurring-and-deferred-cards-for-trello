# Recurring and deferred cards for trello

The purpose of this project is to provide the following functionality:

- Allow creation of trello cards into an "Inbox" list using a simple get request
- Addition of a recurring task concept (whenever a task has recurring date in its description, and it is sent to a "done" list, it is cloned with a new date)
- Addition of a recurring task concept using a label of your choice (default "Repeats" label).
- Addition of a start date concept (whenever a task gets a start date, it is archived until the time it should show up again)
- Optionally get email notifications when a deferred item becomes available.

You can find complete instructions [on my site in a blog I wrote](http://www.sabi.me/2015/12/18/recurring-events-and-deferred-task-for-trello/ "Recurring and deferred cards blog").

Here is a video of the results:

[Video](https://youtu.be/DZWNG-PSG40 "Example Video")

## Legalities

I am not affiliated, associated, authorized, endorsed by or in any way officially connected to Trello, Inc. (www.trello.com).

##Installation:

- Install [node.js] (https://nodejs.org "Node.js site")
- npm install
- If you want to be able to locally test the code run "sudo npm install -g lambda-local". That provides a virtual environment to Lambda AWS.
- Create yourself a key and a token for your application using the [Trello API getting started guide](https://developers.trello.com/get-started "Trello's getting started documentation")
- Copy the config.ini.template to copy.ini
- Populate the config.ini with your information
- Make sure you have your AWS credentials file with chmod 600 in your ~/.aws.
    -- The file needs to be named "credentials"
    -- The content of the file:

        [default]
        aws_access_key_id = <enter your access key for Amazon AWS>
        aws_secret_access_key = <enter your secret access key for Amazon AWS>

##Testing:

###Testing the create end point - use the following from the command line:

`curl -d '{"cardTitle": "this is my content"}' <end point in the API to create a new card>`

###Testing webhooks is very simple:
   -- Use http://requestb.in to create a new webhook (BTW, the inspect page is ?inspect at the end if you "loose it")
   -- `node webHooks.js list` should show you a list of your existing webhooks
   -- `node webHooks.js create "<url of request bin>" "test to be deleted" "<any board name>"` should create a new web hook called deleted that points to the request bin, calling list again should make it show in the list
   -- Once you have the webhook, any changes you make to the board should show on the request bin url you used.
   -- `node webhooks.js delete "<webhook id from the list>` should remove the webhook (run list again)

###Testing the creation of a recurring item:
   -- Create a new card in trello in a board that have "A 'Done' list"
   -- Create a new test webhook like suggested above, specifying the board as well as the 'Done' list
   -- Add text to the description of the card for recurring card (ex: `<~5 days from now~>`)
   -- Drag and drop the card into the done list
   -- Copy the json from the request bin (the latest event) into a file in your project called 'testRecurring.json'
   -- Run the following:

   `lambda-local -l createRecurringItem.js -e testRecurring.json`

   -- You should see any errors on screen, use console.log () to debug

###Testing the creation of a deferred items:
   -- Create a test board (or remove it from archive if you used one before), the reason is because we do not want to use an active board since the existing webhooks may make the check useless.
   -- Create a new card in trello in that board.
   -- Create a new test webhook like suggested above, specifying the board the card is part of.
   -- Add text to the description of the card for deferred card (ex: `<+3 minutes from now+>`)
   -- Copy the json from the request bin (the latest event) into a file in your project called 'testDeferred.json'
   -- Run the following:

   `lambda-local -l archiveDeferredItem.js -e testDeferred.json`

   -- If all goes well, the card should disappear (it should get archived) ... if not, you should see any errors on screen, use console.log () to debug.
   -- To see the card appear, wait a few minutes (in the example above 3 minutes), and run the test for enabling deferred items (see below).

###Testing the enableDeferredItems end point on your local machine, make sure to:
   -- Install lambda-local first
   -- Create a new file named 'emptyInfo.json' with an empty json object {} (the content of the file is simply {})
   -- Execute the following:

   `lambda-local -l enableDeferredItem.js -e emptyInfo.json`

   -- Any deferred items that suppose to be due to show should re-appear. You can check the list in the DynamoDB table (remember that timezones may show you different time than you expect).
   
