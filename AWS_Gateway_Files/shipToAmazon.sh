#!/bin/bash

# removes the last ship file - you may want to change that

rm -Rf amazonShip.zip

zip -r -X amazonShip.zip *.js config.ini "node_modules"

# The AWS sdk module is already part of the lambda function

zip --delete amazonShip.zip "node_modules/aws-sdk/*"

