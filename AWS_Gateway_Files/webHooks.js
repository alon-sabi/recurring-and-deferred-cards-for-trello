/**
	Trello do not have a user interface at this point for webhooks (2015-10-15).
	This file allows one to list, create and delete web hooks
*/


var fs = require('fs')
  , ini = require('ini')
  , Trello = require("node-trello");


var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
var t = new Trello(config.credentials.key,config.credentials.token);


if (typeof(process.argv[2]) == 'undefined') {
	showHelp();
}



var action = process.argv[2];

console.log(action);

switch (action) {
	case 'list': listWebHooks(config.credentials.token);
				break;
	case 'delete':
			if (typeof(process.argv[3]) == 'undefined') {
				showHelp();
			}
			var webHookToDelete = process.argv[3];
			deleteWebHook(webHookToDelete);
			break;
	case 'create':
			if (typeof(process.argv[3]) == 'undefined' || typeof(process.argv[4]) == 'undefined' || typeof(process.argv[5]) == 'undefined') {
				showHelp();
			}

			var webHookUrl = process.argv[3];
			var webHookDescription = process.argv[4];
			var targetBoardForWebHook = process.argv[5];
			var targetListForWebHook = '';
			if (typeof(process.argv[6]) != 'undefined') {
				targetListForWebHook = process.argv[6];
			}

			createWebHook(webHookUrl, targetBoardForWebHook, targetListForWebHook, webHookDescription);
			break;
	default:
	 	showHelp();




}


function deleteWebHook(webHookToDelete) {
	t.del("/1/webhooks/" + webHookToDelete, function(err, data) {
	        console.log(data);
	});
}

function listWebHooks(token) {

    t.get("/1/tokens/" + token + "/webhooks", function(err, data) {
        console.log(data);
    });
}

function createWebHook(webHookUrl, targetBoardForWebHook, targetListForWebHook,webHookDescription) {
t.get("/1/members/me",{boards:"open"}, function(err, data) {

    if (err) throw err;
     // Find the board to insert the task into
     var boardId = findTargetBoardId(data.boards, targetBoardForWebHook);

     t.get("/1/boards/" + boardId + "?lists=open&list_fields=name&fields=lists,labelNames", function(err, lists) {
         if (err) throw err;
         // Find the list to insert the task into (if specified
         if (targetListForWebHook != '') {
             var listId = findTargetListId(lists.lists, targetListForWebHook);
             t.post("/1/webhooks?description=" + webHookDescription + "&callbackURL=" + webHookUrl + "&idModel=" + listId, function (err, data) {
                console.log(data);
             });
         } else {
             t.post("/1/webhooks?description=" + webHookDescription + "&callbackURL=" + webHookUrl + "&idModel=" + boardId, function (err, data) {
                console.log(data);
             });
         }

     });


});
}

// Gets a board name, and a list of boards, and returns the board id (returns null if not)
function findTargetBoardId(boards, boardName) {
   var boardId = null;

   for (boardIndex in boards) {

     if (boards[boardIndex].name == boardName) {
          boardId = boards[boardIndex].id;
     }
   }

   if (boardId == null) {
    throw new Error("Could not find board " + boardName);
   }
   return boardId;
}

// Gets a list name, and a list of lists, and returns the list id (returns null if not)
function findTargetListId(lists, listName) {
   var listId = null;
   for (listIndex in lists) {
     if (lists[listIndex].name == listName) {
          listId = lists[listIndex].id;
     }
   }

   return listId;
}

function showHelp() {
	console.log('*****************************************************************************');
	console.log('* node webHooks.js list                                                     *');
	console.log('* node webhooks.js create <url> <description> <board name> [list name]      *');
	console.log('* node webhooks.js delete <webhook id from list>                            *');
	console.log('*****************************************************************************');
	process.exit();
}
