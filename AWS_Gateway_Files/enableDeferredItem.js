console.log ('loading functions');

require('sugar');

var fs = require('fs')
  , ini = require('ini')
  , Trello = require("node-trello")

  
  
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
var t = new Trello(config.credentials.key,config.credentials.token);

// This function overrides all date manipulations in sugarjs
Date.SugarNewDate = function () {
  var d = new Date();
  // Uncommenting the following line effectively sets newly created dates
  // to GMT-10 hours, the timezone in Honolulu.

  d.addMinutes(d.getTimezoneOffset() - (config.time.offSetFromGMT * 60));
  return d;
};

var aws = require('aws-sdk');

aws.config.update({region:config.deferred.dynamoDbTableRegion});

if (config.aws.accessKeyId !='' && config.aws.secretAccessKey != '') {
    aws.config.update({accessKeyId: config.aws.accessKeyId, secretAccessKey: config.aws.secretAccessKey});
    
}

var ses = new aws.SES({
   region: config.emailNotification.sesRegion 
});

var ddb = new aws.DynamoDB.DocumentClient({params: {TableName: config.deferred.dynamodbTableName}});


exports.handler = function(event, context) {
var requestsInQ = 0;
var currentDateTime = Date.create();

ddb.scan({
        Limit : 100
    }, function(err, data) {
        if (err) { console.log(err); return; }
        console.log(data.id);
         
        for (var ii in data.Items) {
            var currentItem = data.Items[ii];
            //console.log(ii);
            var cardId = currentItem.cardId;
            deferredDate = Date.create(currentItem.deferDate);
            console.log('This is the date we compare to now: ' + deferredDate.long() + '<' +  currentDateTime.long() + " for cardID: " + currentItem.cardId);
            if (deferredDate.isBefore(currentDateTime.long())) {
                console.log("\n==== Removing card from archive: " + currentItem.cardId + " =====\n");
                requestsInQ ++;
                 t.put("/1/cards/" + currentItem.cardId + "?closed=false", function(err, data) {
                        var cardTitle = data.name;
                        var cardDescription = data.desc;
                        console.log(data);
                        if (!err) {
                            // We need to remove the item from the dynamodbb table
                            console.log('=== >>>> Deleting cardid from dynamodb table: ' + data.id)
                            var params =  {
                                            TableName:'trello_deferred',
                                            Key:{
                                            "cardId":data.id
                                            }
                                        }
                            ddb.delete(params, function(err, data) {

                                console.log(err);
                                // We want to exist only when the last entry is completed
                                console.log('Requests in Q:' + requestsInQ);
                                
                                // Emails were setup, so we need to send emails to the recepients
                                if (typeof(config.emailNotification.emailsTo) != 'undefined') {
                                        if (config.emailNotification.emailSubjectPrefix != '') {
                                            var emailSubject = config.emailNotification.emailSubjectPrefix + ' ' + cardTitle;
                                        } else {
                                            var emailSubject = cardTitle;                                            
                                        }
                                    
                                        var doneSendingEmail = false;
                                        var eParams = {
                                            Destination: {
                                                ToAddresses: config.emailNotification.emailsTo
                                            },
                                            Message: {
                                                Body: {
                                                    Text: {
                                                        Data: cardDescription
                                                    }
                                                },
                                                Subject: {
                                                    Data: emailSubject
                                                }
                                            },
                                            Source: config.emailNotification.emailFrom
                                        };
                                        
                                        console.log('===SENDING EMAIL===');
                                        var email = ses.sendEmail(eParams, function(err, data){
                                            if(err) console.log(err);
                                            else {
                                                console.log("===EMAIL SENT===");
                                                console.log(data);
                                            }
                                            requestsInQ --;        
                                
                                            if (!requestsInQ) {
                                                context.succeed(data);
                                                }
                                           
                                        });
                                         
                                } else {
                                
                                    // We do not send an email, so we simply need to decrement the counter
                                    requestsInQ --;        
                                
                                    if (!requestsInQ) {
                                        context.succeed(data);
                                        }
                                }
                            });
                        }
                });
            
        } else {
            console.log('Not yet ...');
        }
        
    }

    if (!requestsInQ) {
        context.succeed(err); 
    }

});
}



