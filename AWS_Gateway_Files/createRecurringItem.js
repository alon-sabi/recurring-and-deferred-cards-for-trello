console.log ('loading functions');

require('sugar');

var fs = require('fs')
  , ini = require('ini')
  , Trello = require("node-trello");


var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
var t = new Trello(config.credentials.key,config.credentials.token);

// This function overrides all date manipulations in sugarjs
Date.SugarNewDate = function () {
  var d = new Date();
  // Uncommenting the following line effectively sets newly created dates
  // to GMT-10 hours, the timezone in Honolulu.

  d.addMinutes(d.getTimezoneOffset() - (config.time.offSetFromGMT * 60));
  return d;
};

exports.handler = function(event, context) {

var cardId = event.action.data.card.id;
var originalList = event.action.data.listBefore.id;
var newList = event.action.data.listAfter.name;
var createItemIfDroppedInto = config.list.done.listName;
var startSymbol = config.recurring.startSymbol;
var endSymbol = config.recurring.endSymbol;
var repeatingLabel = config.recurring.repeatLabel;

if (newList != createItemIfDroppedInto) {
   context.succeed(null);
   process.exit();
}


t.get("/1/cards/"+cardId, function (err, cardInfo) {
  //console.log(cardInfo);
  var str = cardInfo.desc;

  if (str.indexOf(startSymbol) > -1 && str.indexOf(endSymbol) > -1) {
      var recurring = str.substring(str.lastIndexOf(startSymbol)+startSymbol.length,str.lastIndexOf(endSymbol));
      console.log(recurring);

      var newDate = Date.create(recurring);

      if (newDate == 'Invalid Date') {

         // We want to add a friendly message if it does not exist already
         if (cardInfo.desc.indexOf("not a valid date for recurring") == -1) {
             var cardDescription = encodeURIComponent(cardInfo.desc + "\n" + 'Sorry, "' + recurring + '" is not a valid date for recurring, try it on http://sugarjs.com/dates');
          } else {
             var cardDescription = encodeURIComponent(cardInfo.desc);
          }
          t.post("/1/cards?idCardSource=" + cardId + "&desc=" + cardDescription + "&idList=" + originalList, function(err, data) {
                console.log(data);
                context.succeed(err);
           });
          console.log('Sorry, "' + recurring + '" is not a valid date, try it on http://sugarjs.com/dates');
      } else {
          t.post("/1/cards?idCardSource=" + cardId + "&due=" + newDate+ "&idList=" + originalList, function(err, data) {
                console.log(data);
                context.succeed(err);
           });
      }
  } else {
    // Lets check if there is a label that suggests it is a repeating event
    for (index in cardInfo.labels) {
      if (cardInfo.labels[index].name == repeatingLabel) {
         // This card has a repeat label, without a date indication.
         var newDesc = encodeURIComponent(cardInfo.desc + "\nLast completed On: " + Date.create().long());

         t.post("/1/cards?idCardSource=" + cardId + "&idList=" + originalList + "&desc=" + newDesc, function(err, data) {
               console.log(data);
               context.succeed(err);
          });
      }
    }

  }


});
}
