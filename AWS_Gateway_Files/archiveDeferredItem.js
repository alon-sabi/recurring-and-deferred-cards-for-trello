console.log ('loading functions');

require('sugar');

var fs = require('fs')
  , ini = require('ini')
  , Trello = require("node-trello")
  , aws = require('aws-sdk');
  
  
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
var t = new Trello(config.credentials.key,config.credentials.token);

// This function overrides all date manipulations in sugarjs
Date.SugarNewDate = function () {
  var d = new Date();
  // Uncommenting the following line effectively sets newly created dates
  // to GMT-10 hours, the timezone in Honolulu.

  d.addMinutes(d.getTimezoneOffset() - (config.time.offSetFromGMT * 60));
  return d;
};


aws.config.update({region:config.deferred.dynamoDbTableRegion});

if (config.aws.accessKeyId !='' && config.aws.secretAccessKey != '') {
    aws.config.update({accessKeyId: config.aws.accessKeyId, secretAccessKey: config.aws.secretAccessKey});
}

var ddb = new aws.DynamoDB({params: {TableName: config.deferred.dynamodbTableName}});
var deferredStartSymbol = config.deferred.startSymbol;
var deferredEndSymbol = config.deferred.endSymbol;

exports.handler = function(event, context) {

var cardId = event.action.data.card.id;

t.get("/1/cards/"+cardId, function (err, cardInfo) {
  //console.log(cardInfo);

  var str = cardInfo.desc;
  var due = cardInfo.due;
  var cardTitle = cardInfo.name;

  if (str.indexOf(deferredStartSymbol) > -1 && str.indexOf(deferredEndSymbol) > -1) {
      var deferred = str.substring(str.lastIndexOf(deferredStartSymbol) + deferredStartSymbol.length ,str.lastIndexOf(deferredEndSymbol));
      console.log(deferred);

      var newDate = Date.create(deferred);

      if (newDate == 'Invalid Date') {
     
         // We want to add a friendly message if it does not exist already
         if (cardInfo.desc.indexOf("not a valid date for deferring") == -1) {
             var cardDescription = encodeURIComponent(cardInfo.desc + "\n" + 'Sorry, "' + deferred + '" is not a valid date for deferring, try it on http://sugarjs.com/dates');
             
             t.put("/1/cards/" + cardId + "?desc=" + cardDescription, function(err, data) {
                console.log(data);
                context.succeed(err);
           });
          } 

          console.log('Sorry, "' + recurring + '" is not a valid date, try it on http://sugarjs.com/dates');
      } else {
          // We have a valid date
          
          // Check if the date is before or is today, or if the date is after due date, if either of them is true, we do not archive the ticket
          
          if (newDate.isBefore('today')) {
              console.log('The defer date is before today, we ignore it');
              context.succeed(err);
              process.exit();
          }
          
          if (due != null && newDate.isAfter(due)) {
              console.log('The defer date is after the due date, we ignore it');
              context.succeed(err);
              process.exit();
          }
          
          console.log('Good defer date');
          
          // We will remove the defer string 
          // We will add to the description that the item was actually deferred
          // We will store the task in a dynamodb table for future retrieval
          // We will archive the task

          
          var beforeDeferred = str.substring(0, str.lastIndexOf(deferredStartSymbol));
          var afterDeferred = str.substring(str.lastIndexOf(deferredEndSymbol) + deferredEndSymbol.length);
          
          var descWithoutDefer = beforeDeferred + afterDeferred;
          
          var newDesc = encodeURIComponent(descWithoutDefer + "\ndeferred on " + Date.create().long() + " until " + newDate.long());
          
          var itemParams = {
               Item: {
                 cardId: {S: cardId},
                 deferDate: {S: newDate.long()},
                 cardTitle: {S: cardTitle}
               }
            }
               
           ddb.putItem(itemParams, function (err, data) {
                console.log(err);
                console.log(data);
                
                if (!err) {
                    console.log('no errors - changing card');
                    t.put("/1/cards/" + cardId + "?desc=" + newDesc + "&closed=true", function(err, data) {
                        console.log(data);
                        context.succeed(err);
                    });

                }
  });
          
                    
          
           
      }
  } else {
     console.log('not deferred');
     context.succeed(err);
  }


});
}


