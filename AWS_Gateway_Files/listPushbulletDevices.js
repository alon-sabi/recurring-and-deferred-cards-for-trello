
var fs = require('fs')
  , ini = require('ini')
  , Pushbullet = require("pushbullet");
  
  
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

if (config.pushbullet.apiKey != '') {
    var pusher = new Pushbullet(config.pushbullet.apiKey);
    
    pusher.devices(function(error, response) {
        console.log(response);
    });
   
   
}
