
var fs = require('fs')
  , ini = require('ini')
  , Trello = require("node-trello");
  
  
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
var t = new Trello(config.credentials.key,config.credentials.token);

var inboxBoard = config.list.inbox.boardName;
var inboxList = config.list.inbox.listName;


exports.handler = function(event, context) {
    var cardName = encodeURIComponent(event.cardTitle);
    
    t.get("/1/members/me?boards=open", function(err, data) {

        if (err) throw err;

        // Find the board to insert the task into
        var boardId = findTargetBoardId(data.boards, inboxBoard);

        t.get("/1/boards/" + boardId + "?lists=open&list_fields=name&fields=lists,labelNames", function(err, lists) {
           if (err) throw err;
           // Find the list to insert the task into (if specified
           var listId = findTargetListId(lists.lists, inboxList);
           t.post("/1/cards?name=" + cardName + '&idList=' + listId, function (err, data) {
                console.log(data);
                context.succeed(err);
                }
             );
         } 
     );
    });
}

// Gets a board name, and a list of boards, and returns the board id (returns null if not)
function findTargetBoardId(boards, boardName) {
   var boardId = null;
   for (boardIndex in boards) {
     if (boards[boardIndex].name == boardName) {
          boardId = boards[boardIndex].id;
     }
   }

   return boardId;
}

// Gets a list name, and a list of lists, and returns the list id (returns null if not)
function findTargetListId(lists, listName) {
   var listId = null;
   for (listIndex in lists) {
     if (lists[listIndex].name == listName) {
          listId = lists[listIndex].id;
     }
   }

   return listId;
}




