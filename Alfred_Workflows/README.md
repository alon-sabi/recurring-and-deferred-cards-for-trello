# Alfred Workflows:

If you use a Mac, you can use the following Alfred worflows to do the following:

- Check the validity of a date before you use it to defer a card, or make it recurring.
- Once you setup your AWS Api Gateway, you can create new Trello cards from Alfred (by typing "Trello <card description>")

To use the dates, clone the "exploreAllowedDates" into a ~/Documents/dates folder, and execute "npm install" in that folder to get sugar.
